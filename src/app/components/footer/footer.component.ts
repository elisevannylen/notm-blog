import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
    websiteName:string = "Nerd on the Move";
    currentYear:number = new Date().getFullYear();
    socialMediaList:SocialMediaTile[] = [];

    constructor() { }

    ngOnInit(): void {
        let twitter = {icon: 'twitter', url: 'https://twitter.com/EliseVNylen'};
        let facebook = {icon: 'facebook-f', url: 'https://www.facebook.com/elise.vannylen'};
        let instagram = {icon: 'instagram', url: 'https://www.instagram.com/elise_vn_/'};

        this.socialMediaList = [twitter, facebook, instagram];
    }

}

interface SocialMediaTile {
    icon: string;
    url: string;
}
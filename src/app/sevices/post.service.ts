import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from '../models/post.model';
import { PostCover } from '../models/postCover.model';
import { Picture } from '../models/picture.model';
import { Tag } from '../models/tag.model';
import Config from "../config";

@Injectable()
export class PostService {
    constructor(private http: HttpClient) { }
    
    async getPostCovers():Promise<Array<PostCover>>{
		let results = await this.http.get(Config.basesite + "/posts/covers").toPromise();
		return await this.processPostCovers(results);
    }
    
    async getPostCoversByTag(tagId:string):Promise<Array<PostCover>>{
		let results = await this.http.get(Config.basesite + "/posts/coversByTag/" + tagId).toPromise();
		return await this.processPostCovers(results);
    }

    async getPost(postId:string):Promise<Post>{
        let result = await this.http.get(Config.basesite + "/posts/" + postId).toPromise();
        return new Post(result);
    }

    async getFeaturePicture(postId:string):Promise<Picture>{
        let result = await this.http.get(Config.basesite + "/posts/" + postId + "/featurepicture").toPromise();
        let post = new Post(result);
        return new Picture(post.pictures[0]);
	}
    
    private async processPostCovers(results: object):Promise<Array<PostCover>>{
        let postCovers:Array<PostCover> = [];
        for (let i in results){
            let postCover = this.convertPostToPostCover(new Post(results[i]));
            if(postCover.isActive){
                postCover = await this.extendPostCoverWithFeaturePicture(postCover);
                postCovers.push(postCover);
            }
        }
        return this.sortPostCoversByDate(postCovers);
    }
	
	private convertPostToPostCover(post:Post):PostCover{
        let postCover = new PostCover(post.details);
        postCover.postId = post._id;
        postCover.tags = post.tags.map(function(tag:any){ return new Tag(tag); });
        return postCover;
    }
    
    private async extendPostCoverWithFeaturePicture(postCover:PostCover):Promise<PostCover>{
        if(postCover.hasFeaturePicture){
            let picture = await this.getFeaturePicture(postCover.postId);
            postCover.featurePicture = new Picture(picture);
        }
        return postCover;
    }

    private sortPostCoversByDate(postCovers:Array<PostCover>):Array<PostCover>{
        return postCovers.sort(function(a, b){
            let aDate = (a.writeDate != null) ? a.writeDate : a.creationDate;
            let bDate = (b.writeDate != null) ? b.writeDate : b.creationDate;
            return bDate.getTime() - aDate.getTime();
        });
    }
}
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Tag } from '../models/tag.model';
import Config from '../config';

@Injectable()
export class TagService {
    constructor(private http: HttpClient) {}

    async getTags():Promise<Array<Tag>>{
        let results = await this.http.get(Config.basesite + "/tags").toPromise();

        let tags:Array<Tag> = [];
        for(let i in results){
            let tag = new Tag(results[i]);
            tags.push(tag);
        }

        let sortedTags:Array<Tag> = tags.sort((a,b) => {
            return (a.name > b.name) ? 1 : ((a.name < b.name) ? -1 : 0);
        });

        return sortedTags;
    }

    async getTag(tagId:string):Promise<Tag>{
        let result = await this.http.get(Config.basesite + "/tags/" + tagId).toPromise();
        return new Tag(result);
    }
}
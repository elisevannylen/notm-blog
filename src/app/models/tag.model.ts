import * as autoBind from 'auto-bind';

export class Tag{
    _id: string;
    name: string;

    constructor(data:any = null){
        autoBind(this);

        if(data != null){
            this._id = data._id;
            this.name = data.name;
        }
    }
}
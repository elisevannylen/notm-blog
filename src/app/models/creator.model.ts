import * as autoBind from 'auto-bind';

export class Creator{
    _id: string;
    username: string;

    constructor(data:any = null){
        autoBind(this);

        if(data != null){
            this._id = data._id;
            this.username = data.username;
        }
    }
}
import * as autoBind from 'auto-bind';
import { Creator } from './creator.model';
import { Tag } from './tag.model';
import { Picture } from './picture.model';

export class PostCover{
    _id: string;
    title: string;
    description: string;
    writeDate?: Date;
    creationDate: Date = new Date();
    creator: Creator = new Creator();
    isActive: boolean = false;
    hasFeaturePicture: boolean = false;
    featurePicture: Picture = new Picture();
    tags: Tag[] = [];
    postId: string;

    constructor(data:any = null){
        autoBind(this);

        if(data != null){
            this._id = data._id;
            this.title = data.title;
            this.description = data.description;
            this.creationDate = new Date(data.creationDate);
            this.creator = new Creator(data.creator);
            this.isActive = data.isActive;
            this.hasFeaturePicture = data.hasFeaturePicture;

            if(data.writeDate != null || data.writeDate != undefined){
                this.writeDate = new Date(data.writeDate);
            }

            if (data.tags){
                this.tags = data.tags.map(function(tag:any){ return new Tag(tag); });
            }
        }
    }
}